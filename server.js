var http = require("http");
var url = require("url");
var router = require("./routes");
var requestHandlers = require("./requestHandlers");

var handle = {
    "/" : requestHandlers.index,
    "/index.html" : requestHandlers.index,
    "/upload" : requestHandlers.upload,
    "/dbstat" : requestHandlers.dbstat,
    "/ls"     : requestHandlers.start,
    "/greets" : requestHandlers.greets
};

console.log("All started");

function start(route, handle) {

    function onRequest(req,res) {
        var path = url.parse(req.url).pathname;
        console.log("Request for " + path + " received");
        route(handle, path, req, res);
    }

    var ipaddr = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || "127.0.0.1";
    var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080;
    http.createServer(onRequest).listen(port, ipaddr);
    console.log("Keith, server started at " + ipaddr + " port " + port);
}

start(router.route, handle);
