/**
 * Created with JetBrains WebStorm.
 * User: keith
 * Date: 17/06/13
 * Time: 23:12
 * To change this template use File | Settings | File Templates.
 */
var exec = require("child_process").exec;
var fs = require("fs");
var mongo = require("mongodb");

function index(req, res) {
    fs.readFile("public/index.html", function(err,html) {
        if (err) {
            console.log("index gives err " + err);
            res.writeHead(500, {"Content-Type":"text/plain"});
            res.write(err.message);
            res.end();
        } else {
            res.writeHead(200, {"Content-Type":"text/html"});
            res.write(html);
            res.end();
        }
    });
}
function start(req, res) {
    exec("ls -lah", function(error, stdout, stderr) {
        res.writeHead(200, {"Content-Type":"text/plain"});
        res.write(stdout);
        res.end();
    });
}

function upload(req, res) {
    var postData = "";
    req.setEncoding("utf8");
    req.on("data", function(postDataChunk) {
        postData += postDataChunk;
    });
    req.on("end", function() {
       mongo.MongoClient.connect("mongodb://play:vf69R4rUoDDx@ds029798.mongolab.com:29798/play", function(err,db) {
            if (err !== null) { console.log("upload.connect: " + err); return; }
            db.collection("greetings", function(err, greetings) {
                if (err !== null) { console.log("upload.collection: " + err); db.close(); return; }
                var ip = req.headers["x-client-ip"];
                if ( ip === null) {
                    ip = req.headers["x-forwarded-for"];
                    if ( ip === null) {
                        ip = req.connection.remoteAddress;
                    }
                }
                var document = {
                        "when":new Date(),
                        "ip": ip,
                        "hello": postData
                };
                greetings.insert( document, { w : 1 }, function(err, result) {
                        if (err !== null) { console.log("upload.insert: " + err); db.close(); return; }
                        res.writeHead(200, {"Content-Type":"text/plain"});
                        res.write("<div>Inserted</div>")
                        //res.write(JSON.stringify(req.headers,null,"  "));
                        res.end();
                    } ); // insert
            } ); // collection
        } ); // connect
    } ); // end
}

function dbstat(req,res) {
    mongo.MongoClient.connect("mongodb://play:vf69R4rUoDDx@ds029798.mongolab.com:29798/play", function(err,db) {
        if (err !== null) { console.log("dbstat connect: " + err); return; }
        db.stats({}, function(err, stats) {
            if (err !== null) { console.log("dbstat.stats: " + err); db.close(); return; }
            res.writeHead(200, {"Content-Type":"text/plain"});
            res.write(JSON.stringify(stats,null,"  "));
            res.end();
            db.close();
        });
    });
}

function greets(req, res) {
    mongo.MongoClient.connect("mongodb://play:vf69R4rUoDDx@ds029798.mongolab.com:29798/play", function (err, db) {
        if (err !== null) { console.log("greets connect: " + err); return; }
        db.collection("greetings", function (err, greetings) {
            if (err !== null) { console.log("greets.collection: " + err); db.close(); return; }
            greetings.find({}).sort({"when":1}, function (err,cursor) {
                if (err !== null) { console.log("greets.find: " + err); db.close(); return; }
                cursor.toArray( function (err, arr) {
                    if (err !== null) { console.log("greets.toArray: " + err); db.close(); return; }
                    res.writeHead(200, {"Content-Type":"text/plain"});
                    res.write(JSON.stringify(arr,null,"  "));
                    //res.write(JSON.stringify(req.headers,null,"  "));
                    res.end();
                    db.close();

                } );       // toArray
            } ); // find
        } ); // collection
    } ); // connect
}

exports.index = index;
exports.start = start;
exports.upload = upload;
exports.dbstat = dbstat;
exports.greets = greets;