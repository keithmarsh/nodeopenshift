/**
 * Created with JetBrains WebStorm.
 * User: keith
 * Date: 17/06/13
 * Time: 23:03
 * To change this template use File | Settings | File Templates.
 */
function route(handle, path, req, res) {
    if (typeof handle[path] === "function") {
        handle[path](req, res);
    } else {
        console.log("No handler for " + path);
        res.writeHead(404, {"Content-Type":"text/plain"});
        res.write("404 Not found " + path);
        res.end();
    }
}

exports.route = route;